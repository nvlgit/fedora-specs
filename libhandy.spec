%define glib2_version 2.53.0
%define gtk3_version 3.24.1

Name:           libhandy
Version:        0.0.7
Release:        0%{?dist}
Summary:        A library full of GTK+ widgets for mobile phones

License:        LGPL-2.1+
URL:            https://source.puri.sm/Librem5/libhandy
Source0:        https://source.puri.sm/Librem5/%{name}/-/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildRequires:  meson
BuildRequires:  pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires:  pkgconfig(gladeui-2.0)

Requires: gtk3%{?_isa} >= %{gtk3_version}

%description
The aim of The %{name} is to help with developing UI for mobile devices
using GTK+/GNOME.

%package        devel
Summary:        Development package for %{name}

Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       pkgconfig
Requires:       pkgconfig(gtk+-3.0) >= %{gtk3_version}

%description    devel
This package contains development files for %{name}

%package devel-docs
Summary: Developer documentation for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel-docs
This package contains developer documentation for %{name}

%package glade
Summary: Glade data for %{name}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description glade
This package contains Glade data for %{name}

%prep
%autosetup -n %{name}-v%{version}

%build
%meson -Dgtk_doc=true
%meson_build

%install
%meson_install
mv $RPM_BUILD_ROOT%{_bindir}/example $RPM_BUILD_ROOT%{_bindir}/%{name}-demo
rm -rf $RPM_BUILD_ROOT%/usr/lib/debug/usr/bin/*.debug


%files
%license COPYING
%doc AUTHORS HACKING.md README.md
%{_libdir}/%{name}-*.so*
%{_libdir}/girepository-1.0/*.typelib

%files devel
%{_includedir}/%{name}*/*.h
%{_libdir}/pkgconfig/%{name}*.pc
%{_datadir}/gir-1.0/*.gir
%{_datadir}/vala/vapi/%{name}*.vapi
%{_datadir}/vala/vapi/%{name}*.deps
%{_bindir}/%{name}-demo

%files devel-docs
%{_datadir}/gtk-doc

%files glade
%{_datadir}/glade/catalogs/libhandy.xml
%{_libdir}/glade/modules/libglade-handy.so

%changelog
* Sun Jan 20 2019 - v0.0.7
- Initial spec

